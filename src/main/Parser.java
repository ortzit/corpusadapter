package main;

import java.util.ResourceBundle;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import main.utils.Constantes;
import main.utils.FileUtils;

public class Parser {
    private static Searcher searcher = Searcher.createInstance();
    private static String printStr = "";
    private static String printStrIXA = "";
    private static int sentenceCount = -1;
    private static String lang = "";
    
    private static int trainProportion = Integer.parseInt(ResourceBundle.getBundle("config").getString("trainProportion"));
    private static int developmentProportion = Integer.parseInt(ResourceBundle.getBundle("config").getString("developmentProportion"));
    
    public static void parsearDocumento(final Document document, final String fileName, final String langPrefix){
        lang = langPrefix; 
        NodeList nodeList = document.getElementsByTagName("*");
        
        Node node;
        sentenceCount = -1;
        for (int i = 0; i < nodeList.getLength(); i++) {
            node = nodeList.item(i);
            
            //Si es el comienzo de una frase:
            if(node.getNodeName().equals(Constantes.SENTENCE) && node.getAttributes().getNamedItem("title")==null){ //sentence title="yes"
                sentenceCount++;
                
                printStr = Constantes.SALTO_LINEA + sentenceCount + "-".concat(fileName).concat(Constantes.TAB);
                
                //Imprimir
                addToFile(printStr);
                
                //IXA PIPE
                addToFile(Constantes.SALTO_LINEA, true);
            }
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                tratarElemento(node);
            }
        } 
    }
    
    private static void tratarElemento(Node node){
        String value = searcher.search(node, lang);
        
        //Comprobar que se trata de un nombre propio, nombre común o un verbo
        if(node.getAttributes().getNamedItem("lem") != null){
            //Obtener lexname correspondiente (https://wordnet.princeton.edu/man/lexnames.5WN.html)
            
            //sustituir espacios por "_" para evitar casos como "10 de mayo"
            String word = node.getAttributes().getNamedItem("wd").getNodeValue().replace(" ", "_");
            
//            final String lemma = node.getAttributes().getNamedItem("lem").getNodeValue();
            
            String pos = getPOSTag(node);
            String key = value;
            
            boolean isInner = key.contains(Constantes.INNER);
            key = key.replace(Constantes.INNER, "");
            
            String lexname = Lexnames.getLexname(key);
            //Si contiene "_" se trata de multiword
            if(!lexname.equals(Constantes.NONE) && word.contains("_")){
                int i=0;
                String type;
                for(String multiwordPart : word.split("_")){
                    type = i == 0 ? Constantes.BEGINNING : Constantes.INNER;
                    
                    printStr = multiwordPart.concat(" ").concat(pos).concat(" ").concat(type.concat(lexname));
                    //train IXA PIPE
                    printStrIXA = multiwordPart.concat(Constantes.TAB).concat(type.concat(lexname)).concat(Constantes.SALTO_LINEA);
                    
                    addToFile(printStr.concat(Constantes.TAB));
                    //IXA PIPE
                    addToFile(printStrIXA, true);
                    
                    i++;
                }
            }
            else {
                if(lexname.equals(Constantes.NONE)){
                    printStr = word.concat(" ").concat(pos).concat(" ").concat(Constantes.NONE);
                     //IXA PIPE
                    printStrIXA = word.concat(Constantes.TAB).concat(Constantes.NONE_IXA).concat(Constantes.SALTO_LINEA);
                }
                else{
                    printStr = word.concat(" ").concat(pos).concat(" ").concat(isInner ? Constantes.INNER.concat(lexname) : Constantes.BEGINNING.concat(lexname));
                    //IXA PIPE
                    printStrIXA = word.concat(Constantes.TAB).concat(Constantes.BEGINNING.concat(lexname)).concat(Constantes.SALTO_LINEA);
                }
                
                //Imprimir en el fichero (oro NCMS000 B-noun.substance <TAB>)
                addToFile(printStr.concat(Constantes.TAB));
                //IXA PIPE
                addToFile(printStrIXA, true);
            }
        }
    }
    
    private static String getPOSTag(final Node node){
        String pos = null;
        
        //Si hay POS en el nodo, obtenerlo
        if(node.getAttributes().getNamedItem("pos") != null){
            pos = node.getAttributes().getNamedItem("pos").getNodeValue().toUpperCase();
        }
        else{
            //Asignar POS en función al tipo de nodo
//            switch(node.getAttributes().getNamedItem("ne").getNodeValue()){
            switch(node.getNodeName()){
            //Números, fechas e interjecciones
            case Constantes.NODE_NAME_NUMBER:
            case Constantes.NODE_NAME_DATE:
            case Constantes.NODE_NAME_INTERJECTION:
                pos = node.getNodeName();
                break;
            //Signos puntuación
            case Constantes.NODE_NAME_PUNCTUATION:
                //Por error hay algunos elementos del tipo "quotation" sin POS
                pos = getPunctuationPOS(node);
                break;
            case Constantes.NODE_NAME_CONJUNCTION:
                //A la conjuncción "que" (único caso) le corresponde el POS "cs" (conjunción subordinada)
                pos = "cs";
                break;

            case Constantes.NODE_NAME_PREPOSITION:
                //casos: "de", "del", "como"
                break;    
            case Constantes.NODE_NAME_PRONOUN:
                //"cuando", "que", "lo", "otro", "todas"
                break;
            case Constantes.NODE_NAME_NOUN:
                //casos: "indocumentados", "a_priori", "Die_Materie_ist_nicht_anders_-LSB-", "cantante", "Nacional_empezó_pisando_en_falso", "Juegos_Olímpicos"
                break;
            case Constantes.NODE_NAME_ADJETIVE:
                //casos: "engine", "deportados", "fujimoristas"
                break;
            case Constantes.NODE_NAME_ADVERB:
                //casos: "cómo", "a_poco", "hoy"
                break;
            case Constantes.NODE_NAME_DETERMINER:
                //casos: "miles", "Miles", "la"
                break;   
            default:
            }
        }
        if(pos == null){
            pos = Constantes.NONE;
        }
        
        return pos;
    }
    
    private static String getPunctuationPOS(final Node node){
        String pos = null;
        
        if(node.getAttributes().getNamedItem("punct") == null){
            if(node.getAttributes().getNamedItem("wd").getNodeValue().equals(".")){
                pos = Constantes.POS_TAG_FINAL_POINT;
            }
        }
        else{
            switch(node.getAttributes().getNamedItem("punct").getNodeValue()){
            case "quotation":
                pos = Constantes.POS_TAG_QUOTATION;
                break;
            case "bracket":
                pos = node.getAttributes().getNamedItem("wd").getNodeValue().equals("(") ? Constantes.POS_TAG_BRACKET_OPEN : Constantes.POS_TAG_BRACKET_CLOSE;
                break;
            case "period":
                pos = Constantes.POS_TAG_FINAL_POINT;
                break;
            case "hyphen":
                pos = Constantes.POS_TAG_HYPHEN;
                break;    
            default:
            }
        }
        
        return pos;
    }
    
    private static void addToFile(final String str){
        addToFile(str, false);
    }
    
    private static void addToFile(final String str, final boolean ixa){
        final String TRAINING_FILE_PATH = ixa ? Constantes.TRAINING_IXA_FILE_PATH : Constantes.TRAINING_FILE_PATH;
        final String DEVELOPMENT_FILE_PATH = ixa ? Constantes.DEVELOPMENT_IXA_FILE_PATH : Constantes.DEVELOPMENT_FILE_PATH;
        final String TEST_FILE_PATH = ixa ? Constantes.TEST_IXA_FILE_PATH : Constantes.TEST_FILE_PATH;
        
        int index = (sentenceCount % 10) + 1;
        String outputFile = TRAINING_FILE_PATH.replace(Constantes.PUNTO_CENTRAL, lang.concat(Constantes.GUION_BAJO));

        if(index <= trainProportion){
            outputFile = TRAINING_FILE_PATH.replace(Constantes.PUNTO_CENTRAL, lang.concat(Constantes.GUION_BAJO));
        }
        else if(index > trainProportion && index <= (trainProportion + developmentProportion)){
            outputFile = DEVELOPMENT_FILE_PATH.replace(Constantes.PUNTO_CENTRAL, lang.concat(Constantes.GUION_BAJO));
        }
        else{
            outputFile = TEST_FILE_PATH.replace(Constantes.PUNTO_CENTRAL, lang.concat(Constantes.GUION_BAJO));
        }
        
        FileUtils.addToFile(str, outputFile);
    }
}
