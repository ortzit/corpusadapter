package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ResourceBundle;

import main.utils.Constantes;
import main.utils.FileUtils;

public class CorpusSplitter {
    private static final String OUTPUT = Constantes.OUTPUT_PATH;
    
    private static final String OUT_TRAIN_FILE = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_train_file"));
    private static final String OUT_DEVELOPMENT_FILE = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_development_file"));
    private static final String OUT_TEST_FILE = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_test_file"));
    
    private static final String OUT_TRAIN_FILE_IXA = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_train_file_ixa"));
    private static final String OUT_DEVELOPMENT_FILE_IXA = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_development_file_ixa"));
    private static final String OUT_TEST_FILE_IXA = OUTPUT.concat(ResourceBundle.getBundle("config").getString("out_test_file_ixa"));
    
    private static int sentenceCount = -1;
    private static int trainProportion = Integer.parseInt(ResourceBundle.getBundle("config").getString("trainProportion"));
    private static int developmentProportion = Integer.parseInt(ResourceBundle.getBundle("config").getString("developmentProportion"));
    
    public static void main(String[] args) throws IOException {
        String corpusPath = ResourceBundle.getBundle("config").getString("corpus_path");
        BufferedReader br = new BufferedReader(new FileReader(corpusPath));
        
        StringBuilder sbTrain = new StringBuilder();
        StringBuilder sbDevelopment = new StringBuilder();
        StringBuilder sbTest = new StringBuilder();
        
        StringBuilder sbTrainIXA = new StringBuilder();
        StringBuilder sbDevelopmentIXA = new StringBuilder();
        StringBuilder sbTestIXA = new StringBuilder();
        
        //Borrar todos los ficheros de la carpeta
        File outputDir = new File(Constantes.OUTPUT_PATH);
        if(outputDir.listFiles() == null){
            new File((Constantes.OUTPUT_PATH)).mkdirs();  
        }
        else{ 
            FileUtils.deleteFolderFiles(outputDir);
        }
        
        try {
            String sentence = br.readLine();
            String sentenceIXA = "";
            
            while (sentence != null && !sentence.isEmpty()) {
                sentenceIXA = convertSentence2IXAFormat(sentence);
                sentenceCount++;
                
                int index = (sentenceCount % 10) + 1;
                
                if(index <= trainProportion){
                    sbTrain.append(sentence).append(System.lineSeparator());
                    sbTrainIXA.append(sentenceIXA).append(System.lineSeparator());
                }
                else if(index > trainProportion && index <= (trainProportion + developmentProportion)){
                    sbDevelopment.append(sentence).append(System.lineSeparator());
                    sbDevelopmentIXA.append(sentenceIXA).append(System.lineSeparator());
                }
                else{
                    sbTest.append(sentence).append(System.lineSeparator());
                    sbTestIXA.append(sentenceIXA).append(System.lineSeparator());
                }
                
                sentence = br.readLine();
            }
        } finally {
            br.close();
        }

        FileUtils.addToFile(sbTrain.toString(), OUT_TRAIN_FILE);
        FileUtils.addToFile(sbDevelopment.toString(), OUT_DEVELOPMENT_FILE);
        FileUtils.addToFile(sbTest.toString(), OUT_TEST_FILE);
        
        FileUtils.addToFile(sbTrainIXA.toString(), OUT_TRAIN_FILE_IXA);
        FileUtils.addToFile(sbDevelopmentIXA.toString(), OUT_DEVELOPMENT_FILE_IXA);
        FileUtils.addToFile(sbTestIXA.toString(), OUT_TEST_FILE_IXA);
    }
    
    private static String convertSentence2IXAFormat(final String str){
        String sentence = str;
        String sentenceIXA = "";
        
        //Omitir nombre del fichero
        sentence = sentence.substring(sentence.indexOf(Constantes.TAB) + 1);
        
        //Separar por token
        String[] tokens = sentence.split(Constantes.TAB);
        
        //Iterar cada token
        for(String token : tokens){
            //Obtener el token
            sentenceIXA = sentenceIXA.concat(token.split(" ")[0]);
            
            //Tabulación para separar el token del supersense
            sentenceIXA = sentenceIXA.concat(Constantes.TAB);
            
            //Obtener el supersense (para el supersense nulo en IXA se utiliza O en el lugar de 0)
            sentenceIXA = sentenceIXA.concat(token.split(" ")[2].replace(Constantes.NONE, Constantes.NONE_IXA));
            
            //Cada token en una línea
            sentenceIXA = sentenceIXA.concat(Constantes.SALTO_LINEA);
        }
        
        return sentenceIXA;
    }
}
