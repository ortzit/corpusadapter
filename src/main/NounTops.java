package main;

import java.util.HashMap;
import java.util.Map;

import main.utils.Constantes;

public class NounTops {
    private static final String NOUN_TOPS_FILE_NUMBER = "03";
    private static Map<String, Map<String, String>> nounTops = cargarNounTops();
    
    private static Map<String, Map<String, String>> cargarNounTops(){
        Map<String, String> mapEs = new HashMap<String, String>();
        Map<String, String> mapCat = new HashMap<String, String>();
        Map<String, Map<String, String>> map = new HashMap<String, Map<String, String>>();
        
        mapEs.put("conocimiento","09");
        mapEs.put("estado","26");
        mapEs.put("saber","09");
        mapEs.put("organismo","05");
        mapEs.put("fauna","14");
        mapEs.put("lugar","15");
        mapEs.put("grupo","14");
        mapEs.put("flora","14");
        mapEs.put("sustento","04");
        mapEs.put("motivación","09");
        mapEs.put("encapuchado","18");
        mapEs.put("evento","11");
        mapEs.put("cosa","17");
        mapEs.put("sentimiento","12");
        mapEs.put("atributo","07");
        mapEs.put("sustancia","27");
        mapEs.put("posesión","21");
        mapEs.put("espacio","15");
        mapEs.put("comunicación","10");
        mapEs.put("persona","18");
        mapEs.put("agrupación","14");
        mapEs.put("acto","04");
        mapEs.put("colectivo","14");
        mapEs.put("cuantía","23");
        mapEs.put("animal","05");
        mapEs.put("cantidad","23");
        mapEs.put("alimento","13");
        mapEs.put("acción","04");
        mapEs.put("relación","24");
        mapEs.put("causa","16");
        mapEs.put("vida","28");
        mapEs.put("fenómeno","19");
        mapEs.put("objeto","17");
        mapEs.put("artefacto","17");
        mapEs.put("forma","07");
        mapEs.put("célula","08");
        mapEs.put("alma","12");
        mapEs.put("artículo","10");
        mapEs.put("mortal","07");
        mapEs.put("individuo","18");
        mapEs.put("ser","18");
        mapEs.put("criatura","18");
        mapEs.put("planta","20");
        mapEs.put("abstracción","09");
        mapEs.put("tiempo","28");
        mapEs.put("desaparecido","26");
        mapEs.put("humano","18");
        mapEs.put("materia","07");
        mapEs.put("bestia","18");
        mapEs.put("entidad",NOUN_TOPS_FILE_NUMBER);
        
        mapCat.put("coneixement","09");
        mapCat.put("estat","26");
        mapCat.put("saber","09");
        mapCat.put("organismo","05");
        mapCat.put("organisme","14");
        mapCat.put("lloc","15");
        mapCat.put("grup","14");
        mapCat.put("flora","14");
        mapCat.put("suport","04");
        mapCat.put("motivació","09");
        mapCat.put("encaputxat","18");
        mapCat.put("esdeveniment","11");
        mapCat.put("cosa","17");
        mapCat.put("sentiment","12");
        mapCat.put("atribut","07");
        mapCat.put("substància","27");
        mapCat.put("possessió","21");
        mapCat.put("espai","15");
        mapCat.put("comunicació","10");
        mapCat.put("persona","18");
        mapCat.put("agrupació","14");
        mapCat.put("acte","04");
        mapCat.put("col·lectiu","14");
        mapCat.put("quantia","23");
        mapCat.put("animal","05");
        mapCat.put("quantitat","23");
        mapCat.put("aliment","13");
        mapCat.put("acció","04");
        mapCat.put("relació","24");
        mapCat.put("causa","16");
        mapCat.put("vida","28");
        mapCat.put("fenomen","19");
        mapCat.put("objecte","17");
        mapCat.put("artefacte","17");
        mapCat.put("forma","07");
        mapCat.put("cèl·lula","08");
        mapCat.put("ànima","12");
        mapCat.put("article","10");
        mapCat.put("mortal","07");
        mapCat.put("individu","18");
        mapCat.put("ser","18");
        mapCat.put("criatura","18");
        mapCat.put("planta","20");
        mapCat.put("abstracció","09");
        mapCat.put("tiempo","28");
        mapCat.put("temps","26");
        mapCat.put("humà","18");
        mapCat.put("matèria","07");
        mapCat.put("bèstia","18");
        mapCat.put("entitat",NOUN_TOPS_FILE_NUMBER);
        
        map.put(Constantes.ES_PREFIX, mapEs);
        map.put(Constantes.CAT_PREFIX, mapCat);
        return map;
    }
    
    public static String getNounTopReplacement(final String langPrefix, final String lemma){
        String value = "";
        if(nounTops.get(langPrefix).containsKey(lemma)){
            value = nounTops.get(langPrefix).get(lemma);
        }
        else{
            value = NOUN_TOPS_FILE_NUMBER;
        }
        return value;
    }
}