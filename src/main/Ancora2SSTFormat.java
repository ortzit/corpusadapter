package main;

import java.io.File;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;

import main.utils.Constantes;
import main.utils.FileUtils;

public class Ancora2SSTFormat {
    public static Set<String> nounsSet = new HashSet<String>();
    public static Set<String> verbsSet = new HashSet<String>();
    
    private static final String LANGUAGES = ResourceBundle.getBundle("config").getString("lang");
    
    public static void main(String[] args) throws ParserConfigurationException {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        int cont=0;
        try {
            //Borrar todos los ficheros de la carpeta
            File outputDir = new File(Constantes.OUTPUT_PATH);
            if(outputDir.listFiles() == null){
                new File((Constantes.OUTPUT_PATH)).mkdirs();  
            }
            else{ 
                FileUtils.deleteFolderFiles(outputDir);
            }
            
            //Iterar todas las carpetas (castellano y catalán)            
            File parentDir = new File(ResourceBundle.getBundle("config").getString("ancora"));
            File[] directoryListing;
            File[] subDirectoryListing;
            directoryListing = parentDir.listFiles();
            if (directoryListing != null) {
                for (File file : directoryListing) {
                    //Recorrer los subdirectorios
                    if(file.isDirectory() && analyzeDir(file.getName())){
                        subDirectoryListing = file.listFiles();
                        if(subDirectoryListing != null){
                            
                            //Analizar ficheros XML
                            for(File f : subDirectoryListing){
                                if(f.getAbsolutePath().endsWith(".xml")){
                                    System.out.println("Parseando " + f.getAbsolutePath());
                                    Document document = docBuilder.parse(f);
                                    Parser.parsearDocumento(document, f.getName(), getLangPrefix(file.getPath()));
                                    cont++;
                                }
                            }
                        }
                    }
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
        }   
        System.out.println(cont + " ficheros analizados");
        
        for(Entry<String, Set<String>> entry : Searcher.nounsNoSenseSet.entrySet()){
            for(String t : entry.getValue()){
                FileUtils.addToFile(t.concat("\n"), Constantes.OUTPUT_PATH.concat(entry.getKey()).concat("noSenseNouns.txt"));
            }
        }
        for(Entry<String, Set<String>> entry : Searcher.verbsNoSenseSet.entrySet()){
            for(String t : entry.getValue()){
                FileUtils.addToFile(t.concat("\n"), Constantes.OUTPUT_PATH.concat(entry.getKey()).concat("noSenseVerbs.txt"));
            }
        }
        
        System.out.println("En total " + nounsSet.size() + " sustantivos y " + verbsSet.size() + " verbos analizados");
    }
    
    private static boolean analyzeDir(final String name){
        if(LANGUAGES.equals(Constantes.LANG_ALL) ){
            return true;
        }
        else{
            if(LANGUAGES.equals(Constantes.LANG_ES) && name.contains(Constantes.ES_PREFIX) || 
               LANGUAGES.equals(Constantes.LANG_CAT) && name.contains(Constantes.CAT_PREFIX)){
                return true;
            }
            else{
                return false;
            }
        }
    }
    
    private static String getLangPrefix(final String folderName){
        String lang = Constantes.ES_PREFIX;
        
        if(folderName.contains("CAT")){
            lang = Constantes.CAT_PREFIX;
        }
        
        return lang;
    }
}
