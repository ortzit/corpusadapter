package main.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileUtils {

    public static void printInFile(final StringBuffer buffer, final String filePath) {
        printInFile(buffer.toString(), filePath);
    }
    
    public static void printInFile(final String buffer, final String filePath) {
        if(buffer.length() > 0){
            try {
                PrintWriter out = new PrintWriter(filePath);
                out.println(buffer);
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void addToFile(final StringBuffer buffer, final String filePath) {
        addToFile(buffer.toString(), filePath);
    }
    
    public static void addToFile(final String buffer, final String filePath) {
        if(buffer.length() > 0){
            String print = buffer;
            try {
                //Crear fichero si no existe
                File yourFile = new File(filePath);
                if(!yourFile.exists()) {
                    yourFile.createNewFile();
                    if(print.startsWith(Constantes.SALTO_LINEA)){
                        print = print.substring(1);
                    }
                }
                //Añadir contenido
                Files.write(Paths.get(filePath), print.toString().getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void deleteFile(final String filePath){
        try{    
            File file = new File(filePath);
            file.delete();
        } catch(Exception e) {    
            e.printStackTrace();
        }
    }
    
    public static void deleteFolderFiles(File dir) {
        for(File file: dir.listFiles()) {
            file.delete();
        }
    }
}
