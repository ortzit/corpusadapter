package main.utils;

import java.util.ResourceBundle;

public class Constantes {
    
    public static final String TAB = "\t";
    public static final String SALTO_LINEA = "\n";
    public static final String PUNTO_CENTRAL = "·";
    public static final String GUION_BAJO = "_";
    
    public static final String OUTPUT_PATH = ResourceBundle.getBundle("config").getString("output");
    
    public static final String TRAINING_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("train.txt");
    public static final String DEVELOPMENT_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("development.txt");
    public static final String TEST_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("test.txt");
    
    public static final String TRAINING_IXA_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("train_ixa.txt");
    public static final String DEVELOPMENT_IXA_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("development_ixa.txt");
    public static final String TEST_IXA_FILE_PATH = OUTPUT_PATH.concat(PUNTO_CENTRAL).concat("test_ixa.txt");
    
    public static final String LOG_FILE_PATH = OUTPUT_PATH.concat("log.txt");

    public static final String SENTENCE = "sentence";
    public static final String BEGINNING = "B-";
    public static final String INNER = "I-";
    
    public static final String NONE = "0";
    public static final String NONE_IXA = "O";
    
    public static final String NODE_NAME_NOUN = "n";
    public static final String NODE_NAME_VERB = "v";
    public static final String NODE_NAME_ADJETIVE = "a";
    public static final String NODE_NAME_ADVERB = "r";
    public static final String NODE_NAME_PRONOUN = "p";
    public static final String NODE_NAME_DETERMINER = "d";
    public static final String NODE_NAME_PREPOSITION = "s";
    public static final String NODE_NAME_CONJUNCTION = "c";
    public static final String NODE_NAME_INTERJECTION = "i";
    public static final String NODE_NAME_NUMBER = "z";
    public static final String NODE_NAME_DATE = "w";
    public static final String NODE_NAME_PUNCTUATION = "f";
    
    public static final String POS_TAG_FINAL_POINT = "fp";
    public static final String POS_TAG_QUOTATION = "fe";
    public static final String POS_TAG_HYPHEN = "fg";
    public static final String POS_TAG_BRACKET_OPEN = "fpa";
    public static final String POS_TAG_BRACKET_CLOSE = "fpt";
    
    public static final String ES_PREFIX = "CAST";
    public static final String CAT_PREFIX = "CAT";

    public static final String LANG_ES = "es";
    public static final String LANG_CAT = "cat";
    public static final String LANG_ALL = "all";
    
}
