package main;

import java.util.HashMap;
import java.util.Map;

import main.utils.Constantes;

public class Lexnames {
    private static Map<String, String> lexnames = cargarLexnames();
    
    private static Map<String, String> cargarLexnames(){
        Map<String, String> map = new HashMap<String, String>();
        map.put("00", "adj.all");
        map.put("01", "adj.pert");
        map.put("02", "adv.all");
        map.put("03", "noun.Tops");
        map.put("04", "noun.act");
        map.put("05", "noun.animal");
        map.put("06", "noun.artifact");
        map.put("07", "noun.attribute");
        map.put("08", "noun.body");
        map.put("09", "noun.cognition");
        map.put("10", "noun.communication");
        map.put("11", "noun.event");
        map.put("12", "noun.feeling");
        map.put("13", "noun.food");
        map.put("14", "noun.group");
        map.put("15", "noun.location");
        map.put("16", "noun.motive");
        map.put("17", "noun.object");
        map.put("18", "noun.person");
        map.put("19", "noun.phenomenon");
        map.put("20", "noun.plant");
        map.put("21", "noun.possession");
        map.put("22", "noun.process");
        map.put("23", "noun.quantity");
        map.put("24", "noun.relation");
        map.put("25", "noun.shape");
        map.put("26", "noun.state");
        map.put("27", "noun.substance");
        map.put("28", "noun.time");
        map.put("29", "verb.body");
        map.put("30", "verb.change");
        map.put("31", "verb.cognition");
        map.put("32", "verb.communication");
        map.put("33", "verb.competition");
        map.put("34", "verb.consumption");
        map.put("35", "verb.contact");
        map.put("36", "verb.creation");
        map.put("37", "verb.emotion");
        map.put("38", "verb.motion");
        map.put("39", "verb.perception");
        map.put("40", "verb.possession");
        map.put("41", "verb.social");
        map.put("42", "verb.stative");
        map.put("43", "verb.weather");
        map.put("44", "adj.ppl");
        return map;
    }
    
    public static String getLexname(final String key){
        String value = "";
        if(lexnames.containsKey(key)){
            value = lexnames.get(key);
        }
        else{
            value = Constantes.NONE;
        }
        return value;
    }
}