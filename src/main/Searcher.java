package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Stack;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import bean.Ancoranet;
import bean.Link;
import bean.Verbnet;
import bean.Wordnet;
import main.utils.Constantes;
import main.utils.FileUtils;
import main.utils.Funciones;

public class Searcher {
    private static Map<String, String> nouns = new HashMap<String, String>();
    private static Map<String, String> verbs = new HashMap<String, String>();
    private static Map<String, String> newSynsets = new HashMap<String, String>();
    private static Map<String, Set<String>> predicateMatrix = new HashMap<String, Set<String>>();
    private static Map<String, Map<String, String>> ancoranetVerbs = new HashMap<String, Map<String, String>>();
    private static Map<String, Map<String, Integer>> verbsSenses;
    
    private static Searcher searcher;
    public static Map<String, Set<String>> nounsNoSenseSet = new HashMap<String, Set<String>>();
    public static Map<String, Set<String>> verbsNoSenseSet = new HashMap<String, Set<String>>();

    private static String lang;
    
    static Searcher createInstance(){
        if(nouns.isEmpty()){
            searcher = new Searcher();
        }
        return searcher;
    }
    
    private Searcher() {
        //Cargar los ficheros data.noun y data.verb
        cargarWordnetData(ResourceBundle.getBundle("config").getString("wordnet16_data_noun"));
        cargarWordnetData(ResourceBundle.getBundle("config").getString("wordnet30_data_verb"));
        
        //Cargar nuevos synsets que no aparecen en WN 1.6
        cargarNuevosSynsetsData(ResourceBundle.getBundle("config").getString("new_synsets_file"));
        
        //Cargar posibles senses de los verbos (2) (para desambigüar en caso de que un propbank tenga más de un sense)
        verbsSenses = cargarIndexSenseData(ResourceBundle.getBundle("config").getString("wordnet30_index_sense"), 2);
        
        //Cargar contenido Predicate Matrix
        cargarPredicateMatrixData(ResourceBundle.getBundle("config").getString("predicate_matrix_file"));
        
        //Cargar contenido de Ancoranet español
        cargarAncoranetData(ResourceBundle.getBundle("config").getString("ancoranet_es"));
        cargarAncoranetData(ResourceBundle.getBundle("config").getString("ancoranet_cat"));
    }

    public String search(final Node node, final String lang){
        this.lang = lang;
        String value = null;
        String ne = "";
        
        //Solo etiquetar nombres comunes, nombres propios y verbos
        switch(node.getNodeName()){
            case "n":
                Ancora2SSTFormat.nounsSet.add(node.getAttributes().getNamedItem("wd").getNodeValue().replace(" ", "_").toLowerCase());
                
                if(node.getAttributes().getNamedItem("ne") != null){
                    ne = node.getAttributes().getNamedItem("ne").getNodeValue();
                }
                
                if(ne.equals("person") || (node.getAttributes().getNamedItem("pos") != null && node.getAttributes().getNamedItem("pos").getNodeValue().toUpperCase().equals("NP0000P"))){
                    //people
                    //noun.person
                    value = "18";
                }
                else if(ne.equals("organization") || node.getAttributes().getNamedItem("pos") != null && node.getAttributes().getNamedItem("pos").getNodeValue().toUpperCase().equals("NP0000O")){
                    //organizations
                    //noun.group
                    value = "14";
                }
                else if(ne.equals("location") || node.getAttributes().getNamedItem("pos") != null && node.getAttributes().getNamedItem("pos").getNodeValue().toUpperCase().equals("NP0000L")){
                    //locations
                    //noun.location
                    value = "15";
                }
                else{
                    value = procesarNombre(node);
                }
                break;
            case "v":
                Ancora2SSTFormat.verbsSet.add(node.getAttributes().getNamedItem("wd").getNodeValue().replace(" ", "_").toLowerCase());
                value = procesarVerbo(node);
                break;
            case "w":
                //date
                //noun.time
                value = "28";
                break;
            case "z":
                //nombers, percentage, coins
                //noun.quantity
                value = "23";
                break;
                
            default:
                value = Constantes.NONE;
                break;
        }
        
        return value;
    }

    private String procesarNombre(final Node node){
        String sense = "";
        String value = null;
      
        if(node.getAttributes().getNamedItem("sense") != null){
            String lemma = node.getAttributes().getNamedItem("lem").getNodeValue();
            String senseAttr = node.getAttributes().getNamedItem("sense").getNodeValue();
            
            if(senseAttr != null && senseAttr.contains("16:")){
                sense = senseAttr.split(":")[1].replace("n", "");
                        
                //Buscar en el mapa "nouns"
                if(nouns.containsKey(sense)){
                    value = nouns.get(sense);
                }
                else if(newSynsets.containsKey(sense)){
                    value = newSynsets.get(sense);
                }
                else{
                    String msg = "Synset del nombre '" + lemma + "' no encontrado en Wordnet. Synset: " + sense + "\n";
                    FileUtils.addToFile(msg, Constantes.LOG_FILE_PATH);

                    Set<String> storedSet = nounsNoSenseSet.get(lang);
                    if(storedSet == null){
                        storedSet = new HashSet<String>();
                    }
                    storedSet.add(node.getAttributes().getNamedItem("lem").getNodeValue().toLowerCase());
                    nounsNoSenseSet.put(lang, storedSet);
                }

                //Sustituir los noun.Tops 
                if(value != null && value.equals("03")){
                    value = NounTops.getNounTopReplacement(lang, lemma);
                }
            }
        }

        return value == null ? Constantes.NONE : value;
    }
    
    private String procesarVerbo(final Node node) {        
        //Buscar en Ancoranet el sense de Wordnet (en los verbos no se trata de un sense de Wordnet)
        String value = getWordnetSenseFromAncoranet(node);

        //No podemos mirar en newSynsets porque no tenemos el synset de los verbos
        if(value == null){
            String msg = "No se ha podido obtener el synset del verbo con el lema '" + node.getAttributes().getNamedItem("lem").getNodeValue() + "'\n";
            FileUtils.addToFile(msg, Constantes.LOG_FILE_PATH);
            
            //Insertar por idioma
            Set<String> storedSet = verbsNoSenseSet.get(lang);
            if(storedSet == null){
                storedSet = new HashSet<String>();
            }
            storedSet.add(node.getAttributes().getNamedItem("lem").getNodeValue().toLowerCase());
            verbsNoSenseSet.put(lang, storedSet);
        }
        
        return value == null ? Constantes.NONE : value;
    }
    
    //Función que obtiene el código del sense de Wordnet en Ancoranet
    private String getWordnetSenseFromAncoranet(final Node node){
        String lemma = node.getAttributes().getNamedItem("lem").getNodeValue();
        return ancoranetVerbs.get(lang).get(lemma);
    }
    
    private void cargarWordnetData(final String path){
        System.out.println("Cargando " + path);
        
        File file = new File(path);
        String key;
        String value;
        //Comprobar si existe el fichero
        if(file.isFile()){
            FileReader fr;
            try {
                fr = new FileReader (file);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                while((linea=br.readLine())!=null){
                    key = linea.split(" ")[0];
                    if(key.compareTo("") != 0){
                        value = linea.split(" ", 2)[1].split(" ")[0];
                        
                        //Si es un noun cargar en el mapa nouns, si no en verbs
                        if(file.getName().contains("noun")){  
                            nouns.put(key, value);
                        }
                        else if(file.getName().contains("verb")){
                            verbs.put(key, value);
                        }
                    }
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }        
    }
    
    private void cargarNuevosSynsetsData(final String path){
        System.out.println("Cargando " + path);
        
        File file = new File(path);
        String key;
        String value;
        //Comprobar si existe el fichero
        if(file.isFile()){
            FileReader fr;
            try {
                fr = new FileReader (file);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                while((linea=br.readLine())!=null){
                    key = linea.split("-")[0];
                    if(linea.contains(" ") && linea.split(" ").length == 2){
                        value = linea.split(" ")[1];
                        newSynsets.put(key, value);
                    }
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }        
    }
    
    private void cargarPredicateMatrixData(final String path){
        System.out.println("Cargando " + path);
        
        File file = new File(path);
        String key;
        String value;
        //Comprobar si existe el fichero
        if(file.isFile()){
            FileReader fr;
            try {
                fr = new FileReader (file);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                String apartadoVerbo;
                String apartadoSense;
                Set<String> storedSenses;
                while((linea=br.readLine())!=null){
                    if(linea.contains("mcr:")){
//                        System.out.println(linea);
                        key = linea.split(" ")[0];
                        if(key.compareTo("") != 0){
                            apartadoVerbo = linea.split(" ")[2];
                            apartadoSense = linea.split(" ")[0];
                            
                            if(apartadoVerbo.contains(":") && apartadoVerbo.contains(".") && apartadoSense.contains("-")){
                                key = apartadoVerbo.substring(apartadoVerbo.indexOf(":") + 1);
                                value = apartadoSense.split("-", 3)[2].replace("-v", "");
                                
                                if(!value.equals("NULL")){
                                    //Comprobar si ya existía la misma clave almacenada en el mapa 
                                    storedSenses = predicateMatrix.get(key);
                                    if(storedSenses == null){
                                        storedSenses = new HashSet<String>();
                                    }
                                    storedSenses.add(value);
                                    predicateMatrix.put(key, storedSenses);
                                }
                            }
                        }
                    }
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }        
    }
    
    private void cargarAncoranetData(final String path){
        System.out.println("Cargando " + path);        
        try {
            File file = new File(path);
            JAXBContext jaxbContext = JAXBContext.newInstance(Ancoranet.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Ancoranet ancoranet = (Ancoranet) jaxbUnmarshaller.unmarshal(file);
            
            String prefixLang = path.contains("-ca") ? Constantes.CAT_PREFIX : Constantes.ES_PREFIX;
            
            //Cargar fichero en el mapa
            Map<String, Map<String, Integer>> verbsSensesMap = new HashMap<String, Map<String, Integer>>();
            String key = "";
            String ancoralexId = "";
            String propbankId = "";
            List<String> storedAncoralexPropbankList;
            boolean containsWN;

            //KEY: verbo VALUE: propbankid
            Map<String, List<String>> senseAncoralexPropbankMap = new HashMap<String, List<String>>();
            for(Link link : ancoranet.getLinks()){
                containsWN = false;
                ancoralexId = link.getAncoralexid();
                propbankId = link.getPropbankId();
                                
                //Guardar el infinitivo del verbo en la clave
                key = ancoralexId.split("\\.")[1];
                
                storedAncoralexPropbankList = senseAncoralexPropbankMap.get(key);
                if(storedAncoralexPropbankList == null){
                    storedAncoralexPropbankList = new ArrayList<String>();
                }
                storedAncoralexPropbankList.add(ancoralexId.concat(" ").concat(propbankId));
                senseAncoralexPropbankMap.put(key, storedAncoralexPropbankList);
                
                Map<String, Integer> storedSensesMap = null;
                
                
                String sense;
                Integer count;
                for(Verbnet verbnet : link.getVerbnetList()){ 
                    //Incluir en un mapa los posibles senses dentro de cada elemento "verbnet"
                    for(Wordnet wordnet : verbnet.getWordnetList()){
                      //Tiene contenido de WN, por lo tanto recorrer cada posible sense en WN
                        if(!wordnet.getText().isEmpty() && wordnet.getText().contains(":")){
                            containsWN = true;
                            //Obtener sense
                            sense = wordnet.getText().split(":")[1];
                            
                            //Añadir sense en el mapa
                            storedSensesMap = verbsSensesMap.get(key);
                            if(storedSensesMap == null){
                                storedSensesMap = new HashMap<String, Integer>();
                            }
                            count = storedSensesMap.get(sense);
                            if(count == null){
                                count = 0;
                            }
                            count++;
                            storedSensesMap.put(sense, count);
                        }
                    }                   
                }
                
                //Si NO hay contenido de WN buscar en el Predicate Matrix
                if(!containsWN){
                    
                    //Comprobar si el propbank existe en el Predicate Matrix
                    if(predicateMatrix.containsKey(propbankId)){
                        String selectedSense = "";
                        
                        //Obtener propbankId y buscar en el mapa predicateMatrix
                        Set<String> storedSenses = predicateMatrix.get(propbankId);
                        
                        //Obtener lista de los posibles senses distintos
                        Set<String> fileNumbers = new HashSet<String>();
                        for(final String storedSense : storedSenses){
                            fileNumbers.add(verbs.get(storedSense).split(" ")[0]);
                        }
                        
                        //Si hay más de un sense quedarse con el más frecuente
                        if(fileNumbers.size() > 1){
                            String termino;
                            int maxValue = -1;
                            for(String fileNumber : fileNumbers){
                                termino = propbankId.split("\\.")[0];

                                if(verbsSenses.containsKey(termino) && verbsSenses.get(termino).containsKey(fileNumber) 
                                        && verbsSenses.get(termino).get(fileNumber) > maxValue){
                                    maxValue = verbsSenses.get(termino).get(fileNumber);
                                    selectedSense = fileNumber;
                                }
                            }
                            //Si no se ha podido seleccionar ningún sense asignar el primero
                            if(selectedSense.equals("")){
                                selectedSense = fileNumbers.iterator().next();
                            }
                        }
                        else{
                            selectedSense = fileNumbers.iterator().next();
                        }
                                
                        //Añadir sense en el mapa
                        storedSensesMap = verbsSensesMap.get(key);
                        if(storedSensesMap == null){
                            storedSensesMap = new HashMap<String, Integer>();
                        }
                        count = storedSensesMap.get(selectedSense);
                        if(count == null){
                            count = 0;
                        }
                        count++;
                        storedSensesMap.put(selectedSense, count);      
                    }
                }
                //Guardar en el mapa
                if(storedSensesMap != null || (storedSensesMap == null && !verbsSensesMap.containsKey(key))){
                    verbsSensesMap.put(key, storedSensesMap);
                }
            }
            
            
            //Pasar al mapa principal obteniendo únicamente el sense mayoritario de cada verbo
            String senseMayoritario = "";
            StringBuffer notFound = new StringBuffer();
            Set<String> notMappedVerbs = new HashSet<String>();
            int sinSense = 0;
            Map<String, String> storedMap;
            for(Entry<String, Map<String, Integer>> entry : verbsSensesMap.entrySet()){
                if(entry.getValue() != null){
                    senseMayoritario = getSenseMayoritario(entry.getValue());
                    
                    storedMap = ancoranetVerbs.get(prefixLang);
                    if(storedMap == null){
                        storedMap = new HashMap<String, String>();
                    }
                    storedMap.put(entry.getKey(), senseMayoritario);
                    ancoranetVerbs.put(prefixLang, storedMap);
                }
                else{
                    notMappedVerbs.add(entry.getKey());
                    storedAncoralexPropbankList = senseAncoralexPropbankMap.get(entry.getKey());
                    for(final String ancorelexPropbank : storedAncoralexPropbankList){
                        notFound.append(ancorelexPropbank).append("\n");
                        sinSense++;
                    }
                }
            }
            
            //Imprimir formas de verbos sin sense en el fichero de log
//            FileUtils.printInFile(notFound, Constantes.LOG_FILE_PATH);
            
            System.out.println("Se han cargado " + ancoranetVerbs.get(prefixLang).size() + " verbos de Ancoranet_" + prefixLang + " (" + notMappedVerbs.size() + " verbos sin sense, en total " + sinSense + " senses)");     
        } catch (JAXBException e) {
            System.out.println("No se ha podido cargar el fichero de ancoranet ".concat(path));
            e.printStackTrace();
        }
    }
    
    private Map<String, Map<String, Integer>> cargarIndexSenseData(final String path, final Integer syntacticCategory){
        System.out.println("Cargando " + path);
        
        Map<String, Map<String, Integer>> senses = new HashMap<String, Map<String, Integer>>();
        File file = new File(path);
        String propbankKey;
        String senseKey;
        Integer value;
        //Comprobar si existe el fichero
        if(file.isFile()){
            FileReader fr;
            try {
                fr = new FileReader (file);
                BufferedReader br = new BufferedReader(fr);
                String linea;
                String seccionTermino;
                Integer currentSc;
                Map<String, Integer> storedSenses;
                
                while((linea=br.readLine()) != null){                    
                    seccionTermino = linea.split(" ")[0];
                    currentSc = Integer.parseInt(seccionTermino.substring(seccionTermino.indexOf("%") + 1, seccionTermino.indexOf(":")));
                    
                    //Si el termino es de la categoría sintáctica requerida
                    if(syntacticCategory.equals(currentSc)){
                        propbankKey = seccionTermino.substring(0, seccionTermino.indexOf("%"));
                        
                        senseKey = seccionTermino.split(":")[1];
                        value = Integer.parseInt(linea.split(" ")[3]);
                            
                        storedSenses = senses.get(propbankKey);
                        if(storedSenses == null){
                            storedSenses = new HashMap<String, Integer>();
                        }
                        
                        Integer storedCurrency = storedSenses.get(senseKey);
                        if(storedCurrency == null){
                            storedCurrency = 0;
                        }
                        
                        storedSenses.put(senseKey, storedCurrency + value);
                        senses.put(propbankKey, storedSenses);                        
                    }
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
        
        return senses;
    }
    
    private static String getSenseMayoritario(Map<String, Integer> senses){
        int mayoritarioCount = 0;
        String senseMayoritario = "";
        for(Entry<String, Integer> entry : senses.entrySet()){
             if(entry.getValue() > mayoritarioCount){
                 senseMayoritario = entry.getKey();
                 mayoritarioCount = entry.getValue();
             }
        }
        return senseMayoritario;
    }
}
