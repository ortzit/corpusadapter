package bean;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Verbnet {

    List<Wordnet> wordnetList = new ArrayList<Wordnet>();

    public List<Wordnet> getWordnetList() {
        return wordnetList;
    }
 
    @XmlElement( name = "wordnet" )
    public void setWordnetList(List<Wordnet> wordnetList) {
        this.wordnetList = wordnetList;
    }
}
