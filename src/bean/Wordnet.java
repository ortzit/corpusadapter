package bean;

import javax.xml.bind.annotation.XmlValue;

public class Wordnet {
    private String text;

    public String getText() {
        return text;
    }

    @XmlValue
    public void setText(String text) {
        this.text = text;
    }
    
}
