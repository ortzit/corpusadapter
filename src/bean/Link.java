package bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Link {
    String ancoralexid = "";
    List<Verbnet> verbnetList = new ArrayList<Verbnet>();
    String propbankId = "";
    
    public String getAncoralexid() {
        return ancoralexid;
    }
    
    @XmlAttribute( name = "ancoralexid" )
    public void setAncoralexid(String ancoralexid) {
        this.ancoralexid = ancoralexid;
    }
    
    public List<Verbnet> getVerbnetList() {
        return verbnetList;
    }
    
    @XmlElement( name = "verbnet" )
    public void setVerbnetList(List<Verbnet> verbnetList) {
        this.verbnetList = verbnetList;
    }

    public String getPropbankId() {
        return propbankId;
    }

    @XmlAttribute( name = "propbankid" )
    public void setPropbankId(String propbankId) {
        this.propbankId = propbankId;
    }

    
}
