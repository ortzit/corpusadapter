package bean;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement( name = "ancoranet" )
public class Ancoranet {

    List<Link> links = new ArrayList<Link>();

    public List<Link> getLinks() {
        return links;
    }

    @XmlElement( name = "link" )
    public void setLinks(List<Link> links) {
        this.links = links;
    }
    
}
